from .event import Event2, Event3

class AttackEvent(Event2):
    NAME  = "attack"
    def perform(self):
        inventaire = [nom.id for nom in self.actor.contents()]
        if "epee-000" not in inventaire and "epee-or-000" not in inventaire:
            self.inform("attack.failed") # changer ces lignes pour avoir des cas plus précis
            self.executeEffect("attack.failed")
        elif self.object.has_prop("armor-needed"):
            if "armor-000" not in [nom.id for nom in self.actor.contents()]:
                self.inform("attack.failed")
                self.executeEffect("attack.failed")
            elif "epee-or-000" in inventaire:
                self.inform("attack.armor-with-golden")
                self.executeEffect("attack.armor-with-golden")
            else:
                self.inform("attack.armor-with-simple")
                self.executeEffect("attack.armor-with-simple")
        else:
            self.inform("attack.only-simple-sword")
            self.executeEffect("attack.only-simple-sword")
    
    def executeEffect(self,path): # permet d'éxécuter des events dans un path différent
        for effect in self.get_effects(path):
            effect.execute()

class AttackWithEvent(Event3):
    NAME  = "attack-with"
    def perform(self):
        print([x for x in self.get_effects("attack-with")])
        if self.object.has_prop("fought-without-armor"): # Si il y a plusieurs personnes
            self.object.remove_prop("fought-without-armor")
        if self.object.has_prop("fought-without-orbe"): # Si il y a plusieurs personnes
            self.object.remove_prop("fought-without-orbe")
        if self.object.has_prop("armor-needed"):
            if "armor-000" not in [nom.id for nom in self.actor.contents()]:
                self.object.add_prop("fought-without-armor")
        if self.object.has_prop("armor-needed"):
            if "orbe-000" not in [nom.id for nom in self.actor.contents()]:
                self.object.add_prop("fought-without-orbe")
        self.inform("attack-with")